#include <WiFi.h>
#include <Arduino.h>
#include <ThingsBoard.h>
#include <HardwareSerial.h>
#include "EEPROM.h"
#include "String.h"

#define RED_LED_PIN 4
#define GREEN_LED_PIN 2
#define RXD2 16
#define TXD2 17

#define VOLTAGE_230_110_PIN 34
#define DIP_1_PIN 26
#define DIP_2_PIN 25
#define DIP_3_PIN 33
#define DIP_4_PIN 32

//#define DEBUG_DECODER
//#define DEBUG_WIFI

#define EEPROM_SIZE 512

#define TOKEN "nathamdenathamdenaanaa"

#define EEPROM_SSID_LENGTH_ADDR 0x00
#define EEPROM_SSID_ADDR 0x04
#define EEPROM_PASSWORD_LENGTH_ADDR 0xC8  //200
#define EEPROM_PASSWORD_ADDR 0xCC         //204

#define MAX_RETRY_COUNT 30 //For Connecting WiFi
#define MAX_TB_RETRY_COUNT 2 //For Connecting TB

char thingsboardServer[] = "thingsboard.cloud";

WiFiClient wifiClient;

ThingsBoard tb(wifiClient);

HardwareSerial MCUSerial(2);

int status = WL_IDLE_STATUS;

enum LEDColor {
  GREEN = (uint8_t)0,
  RED = (uint8_t)1
};

enum Connectivity {
  WIFI_NOT_CONNECTED,
  WIFI_CONNECTED,
  TB_NOT_CONNECTED,
  TB_CONNECTED
};

struct sensorVal_s {
  float temperature = 0.0;
  float humidity = 0.0;
  bool door_1 = false;
  bool door_2 = false;
  bool fire = false;
  bool water = false;
  bool gsm = false;
  float battery = 0.0;
  bool ac_in = false;
  bool ups_in = false;
  float ac_current = 0.0;
  float ups_current = 0.0;
  float ac_power = 0.0;
  float ups_power = 0.0;
};

struct switchState_s{
  bool voltage_230_110 = false;
  bool dip_1 = false;
  bool dip_2 = false;
  bool dip_3 = false;
  bool dip_4 = false;
};

void decodeSerial(sensorVal_s* sensor_val, bool* send_now_hit);
void saveWiFiCredentials(char* wifi_ssid, uint8_t ssid_length, char* wifi_password, uint8_t password_length);
Connectivity getSavedWiFiCredentialsAndConnect();
void getSwitchState(switchState_s* switch_state);
void sendDataNow(sensorVal_s* sensor_val);
void LEDInit(void);
void setLED(Connectivity state);

sensorVal_s g_sensor_val;
switchState_s g_switchState;
bool g_is_send_now = false;
Connectivity g_connectivity = WIFI_NOT_CONNECTED;
const char* g_wifi_ssid;
const char* g_wifi_password;

void setup() {
  LEDInit();
  EEPROM.begin(EEPROM_SIZE);
  delay(1000);
  Serial.begin(115200);
  delay(100);

  MCUSerial.begin(9600, SERIAL_8N1, RXD2, TXD2);

  getSwitchState(&g_switchState);
  sendSwitchState(&g_switchState);

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(1000);

  g_connectivity = getSavedWiFiCredentialsAndConnect();
  setLED(g_connectivity);

  if(g_connectivity == WIFI_CONNECTED){    
    g_connectivity = reconnectTB();
    setLED(g_connectivity);    
  }
}

void loop() {
  while (MCUSerial.available()) {
    decodeSerial(&g_sensor_val, &g_is_send_now);
  }
  //MCUSerial.flush();

  do {
    if (g_is_send_now == true) {
      status = WiFi.waitForConnectResult(1000);
      if (status != WL_CONNECTED) {
        Serial.println("WIFI_NOT_CONNECTED_INITIAL");
        //first try to connect to WiFi
        g_connectivity = getSavedWiFiCredentialsAndConnect();
        setLED(g_connectivity);
      }

      if (status == WL_CONNECTED) {
        Serial.println("WIFI_CONNECTED");
        //then try to connect to TB
        g_connectivity = reconnectTB();
        setLED(g_connectivity);
      } else {
        Serial.println("WIFI_NOT_CONNECTED");
        //if WIFI not connected
        break;
      }

      if (g_connectivity == TB_CONNECTED) {
        Serial.println("SENDING");
        //then try to send data
        sendDataNow(&g_sensor_val);
        sendSentConfirmation();
        g_is_send_now = false;
      } else {
        Serial.println("TB_NOT_CONNECTED");
        //if TB not connected
        break;
      }
    }
  } while (false);
}


Connectivity getSavedWiFiCredentialsAndConnect(void) {
  String ssid_saved_str = "";
  String password_saved_str = "";

  uint8_t ssid_get_length;
  EEPROM.get(EEPROM_SSID_LENGTH_ADDR, ssid_get_length);
  ssid_get_length -= 1;
  char ssid_get_char[ssid_get_length];
  for (uint8_t i = 0; i < ssid_get_length; i++) {
    EEPROM.get(i + EEPROM_SSID_ADDR, ssid_get_char[i]);
    ssid_saved_str.concat(ssid_get_char[i]);
  }
  const char* ssid_temp = ssid_saved_str.c_str();
  g_wifi_ssid = ssid_temp;
  //g_wifi_ssid = "Chameera_UoM";

  uint8_t password_get_length;
  EEPROM.get(EEPROM_PASSWORD_LENGTH_ADDR, password_get_length);
  password_get_length -= 1;
  char password_get_char[password_get_length];
  for (uint8_t i = 0; i < password_get_length; i++) {
    EEPROM.get(i + EEPROM_PASSWORD_ADDR, password_get_char[i]);
    password_saved_str.concat(password_get_char[i]);
  }
  const char* password_temp = password_saved_str.c_str();
  g_wifi_password = password_temp;
  //g_wifi_password = "19960510";

#ifdef DEBUG_WIFI
  Serial.print("wifi_ssid_saved: ");
  Serial.println(g_wifi_ssid);
  Serial.print("wifi_password_saved: ");
  Serial.println(g_wifi_password);
  delay(1000);
#endif

  WiFi.mode(WIFI_STA);
  WiFi.begin(g_wifi_ssid, g_wifi_password);
  Serial.println("Connecting to AP ->");  // attempt to connect to WiFi network

  uint8_t retry_count = 0;

  while (WiFi.status() != WL_CONNECTED) {
    if (retry_count > MAX_RETRY_COUNT) {
      Serial.println("NOT CONNECTED AP...");
      return WIFI_NOT_CONNECTED;
    }
    retry_count += 1;
    delay(1000);
    Serial.print(WiFi.status());
  }
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("CONNECTED AP!!!");
    return WIFI_CONNECTED;
  }
  return WIFI_NOT_CONNECTED;

  /*WiFi.mode(WIFI_STA);
  WiFi.begin(g_wifi_ssid, g_wifi_password);
  Serial.print("Connecting to WiFi ..");
  int count = 0;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
    count++;
    if (count > 10){break;}
  }
  Serial.println(WiFi.localIP());
  return WIFI_NOT_CONNECTED;*/
}

void sendDataNow(sensorVal_s* sensor_val) {
  Serial.println("Sending Data");
  tb.sendTelemetryFloat("temp", sensor_val->temperature);
  tb.sendTelemetryFloat("hum", sensor_val->humidity);
  tb.sendTelemetryFloat("bat", sensor_val->battery);
  tb.sendTelemetryFloat("d_1", sensor_val->door_1);
  tb.sendTelemetryFloat("d_2", sensor_val->door_2);
  tb.sendTelemetryFloat("fire", sensor_val->fire);
  tb.sendTelemetryFloat("water", sensor_val->water);
  tb.sendTelemetryFloat("ac_in", sensor_val->ac_in);
  tb.sendTelemetryFloat("ac_pow", sensor_val->ac_power);
  tb.sendTelemetryFloat("ups_in", sensor_val->ups_in);
  tb.sendTelemetryFloat("ups_pow", sensor_val->ups_power);
  tb.loop();
}

Connectivity reconnectTB(void) {
  uint8_t retry_count = 0;
  Serial.print("Connecting to ThingsBoard node ...");

  if(tb.connected())
  {
    return TB_CONNECTED;
  }

  while (!tb.connected()) {
    if (retry_count > MAX_TB_RETRY_COUNT) {
      Serial.println("NOT CONNECTED TB...");
      return TB_NOT_CONNECTED;
    }
    if (tb.connect(thingsboardServer, TOKEN)) {
      Serial.println("CONNECTED TB...");
      return TB_CONNECTED;
    } else {
      Serial.print("[FAILED] Attempt TB: ");
      Serial.println(retry_count);
      delay(5000);
    }
    retry_count += 1;
  }
  return TB_NOT_CONNECTED;
}

void LEDInit(void) {
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
  setLED(g_connectivity);
}

void setLED(Connectivity state) {
  //write HIGH to turn ON
  //write LOW to turn OFF
  if (state == WIFI_NOT_CONNECTED)  //RED
  {
    digitalWrite(GREEN_LED_PIN, LOW);
    digitalWrite(RED_LED_PIN, HIGH);
  } else if (state == WIFI_CONNECTED || state == TB_NOT_CONNECTED)  //AMBER
  {
    digitalWrite(GREEN_LED_PIN, HIGH);
    digitalWrite(RED_LED_PIN, HIGH);
  } else if (state == TB_CONNECTED)  //GREEN
  {
    digitalWrite(GREEN_LED_PIN, HIGH);
    digitalWrite(RED_LED_PIN, LOW);
  }
}

void getSwitchState(switchState_s* switch_state)
{
  pinMode(VOLTAGE_230_110_PIN, INPUT);
  pinMode(DIP_1_PIN, INPUT);
  pinMode(DIP_2_PIN, INPUT);
  pinMode(DIP_3_PIN, INPUT);
  pinMode(DIP_4_PIN, INPUT);

  switch_state->voltage_230_110 = (bool)digitalRead(VOLTAGE_230_110_PIN); // 0: 230, 1: 110 
  switch_state->dip_1 = (bool)digitalRead(DIP_1_PIN);
  switch_state->dip_2 = (bool)digitalRead(DIP_2_PIN);
  switch_state->dip_3 = (bool)digitalRead(DIP_3_PIN);
  switch_state->dip_4 = (bool)digitalRead(DIP_4_PIN);
}
