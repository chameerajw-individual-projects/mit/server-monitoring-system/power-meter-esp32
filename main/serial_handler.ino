#ifndef SERIAL_HANDLER_H
#define SERIAL_HANDLER_H


/*
t3070 h6900 x0 y0 f0 w0 b422 a0 p0 u0 o0
a: ac_in
b: battery
c: confirm
f: fire
h: humidity
o: ups_power
p: ac_power
s: send_now
t: temperature
u: ups_in
w: water
x: door_1
y: door_2
z: SSID, PASSWORD
*/


void decodeSerial(sensorVal_s* sensor_val, bool* send_now_hit) {
  char command = MCUSerial.read();
  switch (command) {
    /*********************** AC_IN ****************/
    case 'a':
      sensor_val->ac_in = (bool)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->ac_in: ");
      Serial.println(sensor_val->ac_in);
#endif
      break;

    /*********************** BATTERY ****************/
    case 'b':
      sensor_val->battery = (int)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->battery: ");
      Serial.println(sensor_val->battery, 4);
#endif
      break;

    /*********************** FIRE ****************/
    case 'f':
      sensor_val->fire = (bool)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->fire: ");
      Serial.println(sensor_val->fire);
#endif
      break;

    /*********************** HUMIDITY ****************/
    case 'h':
      sensor_val->humidity = (float)(MCUSerial.parseFloat() / 100);
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->humidity: ");
      Serial.println(sensor_val->humidity, 4);
#endif
      break;

    /*********************** UPS_POWER ****************/
    case 'o':
      sensor_val->ups_power = (float)(MCUSerial.parseInt());
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->ups_power: ");
      Serial.println(sensor_val->ups_power, 4);
#endif
      break;

    /*********************** AC_POWER ****************/
    case 'p':
      sensor_val->ac_power = (float)(MCUSerial.parseInt());
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->ac_power: ");
      Serial.println(sensor_val->ac_power, 4);
#endif
      break;

    /*********************** SEND NOW ****************/
    case 's':
      *send_now_hit = true;
      //MCUSerial.println('c');  //confirmation of complete message. this will be sent after sending to TB
#ifdef DEBUG_DECODER
      Serial.print("SEND NOW!!");
      Serial.println();
#endif
      break;

    /*********************** TEMPERATURE ****************/
    case 't':
      sensor_val->temperature = (float)(MCUSerial.parseFloat() / 100);
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->temperature: ");
      Serial.println(sensor_val->temperature, 4);
#endif
      break;

    /*********************** UPS_IN ****************/
    case 'u':
      sensor_val->ups_in = (bool)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->ups_in: ");
      Serial.println(sensor_val->ups_in);
#endif
      break;

    /*********************** WATER ****************/
    case 'w':
      sensor_val->water = (bool)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->water: ");
      Serial.println(sensor_val->water);
#endif
      break;

    /*********************** DOOR_1 ****************/
    case 'x':
      sensor_val->door_1 = (bool)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->door_1: ");
      Serial.println(sensor_val->door_1);
#endif
      break;

    /*********************** DOOR_2 ****************/
    case 'y':
      sensor_val->door_2 = (bool)MCUSerial.parseInt();
#ifdef DEBUG_DECODER
      Serial.print("sensor_val->door_2: ");
      Serial.println(sensor_val->door_2);
#endif
      break;

    /*********************** WIFI ****************/
    case 'z':
      String wifi_credentials = MCUSerial.readString();
      uint8_t comma_position = wifi_credentials.indexOf(",");
      String wifi_ssid = wifi_credentials.substring(0, comma_position);
      String wifi_password = wifi_credentials.substring(comma_position + 1, wifi_credentials.length() - 1);

      uint8_t ssid_length = wifi_ssid.length() + 1;
      char wifi_ssid_char[ssid_length];
      wifi_ssid.toCharArray(wifi_ssid_char, ssid_length);

      uint8_t password_length = wifi_password.length();
      char wifi_password_char[password_length];
      wifi_password.toCharArray(wifi_password_char, password_length);

      saveWiFiCredentials(wifi_ssid_char, ssid_length, wifi_password_char, password_length);

#ifdef DEBUG_DECODER
      Serial.print("wifi_ssid_char: ");
      Serial.println(wifi_ssid_char);
      Serial.print("wifi_password_char: ");
      Serial.println(wifi_password_char);
      delay(1000);
#endif
      ESP.restart();  //restart to connect to WiFi
      break;
  }
}

void sendSwitchState(switchState_s* switch_state)
{
  /*
  Serial.println("PRINTING SWITCH STATE");
  Serial.println(switch_state->voltage_230_110);
  Serial.println(switch_state->dip_1);
  Serial.println(switch_state->dip_2);
  Serial.println(switch_state->dip_3);
  Serial.println(switch_state->dip_4);  
  */

  /*char esp_message[10];
  sprintf(esp_message, "x%da%db%dc%dd%d",
          (int)(switch_state->voltage_230_110),
          (int)(switch_state->dip_1),
          (int)(switch_state->dip_2),
          (int)(switch_state->dip_3),
          (int)(switch_state->dip_4));
  
  MCUSerial.println(esp_message);
  delay(2000);*/

  MCUSerial.print('x');
  MCUSerial.print((int)(switch_state->voltage_230_110)); // 0: 230, 1: 110 

  MCUSerial.print('a');
  MCUSerial.print((int)(switch_state->dip_1));

  MCUSerial.print('b');
  MCUSerial.print((int)(switch_state->dip_2));

  MCUSerial.print('c');
  MCUSerial.print((int)(switch_state->dip_3));

  MCUSerial.print('d');
  MCUSerial.println((int)(switch_state->dip_4));

  delay(2000);  

}


void saveWiFiCredentials(char* wifi_ssid, uint8_t ssid_length, char* wifi_password, uint8_t password_length) {
  EEPROM.put(EEPROM_SSID_LENGTH_ADDR, ssid_length);
  EEPROM.commit();
  delay(1000);

  for (uint8_t i = 0; i < ssid_length; i++) {
    EEPROM.put(i + EEPROM_SSID_ADDR, wifi_ssid[i]);
  }
  EEPROM.commit();
  delay(1000);

  EEPROM.put(EEPROM_PASSWORD_LENGTH_ADDR, password_length);
  EEPROM.commit();
  delay(1000);

  for (uint8_t i = 0; i < password_length; i++) {
    EEPROM.put(i + EEPROM_PASSWORD_ADDR, wifi_password[i]);
  }
  EEPROM.commit();
  delay(1000);
}

void sendSentConfirmation(void)
{
  MCUSerial.println('c'); //confirmation character
  delay(5000);
  MCUSerial.println('c'); //confirmation character
}

#endif